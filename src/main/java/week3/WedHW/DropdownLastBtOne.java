package week3.WedHW;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class DropdownLastBtOne {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver d=new ChromeDriver();
		d.manage().window().maximize();
		d.get("http://leafground.com/pages/Dropdown.html");
		WebElement drop = d.findElementById("dropdown1");
		Select dropDown=new Select(drop);
		List<WebElement> dropOptions = dropDown.getOptions();
		dropOptions.size();
		dropOptions.lastIndexOf(dropOptions);
		dropDown.selectByIndex(dropOptions.size()-2);
		//System.out.println(dropOptions.lastIndexOf(dropOptions));
		for (WebElement eachOp : dropOptions) {
			System.out.println(eachOp.getText());
		}

	}

}
