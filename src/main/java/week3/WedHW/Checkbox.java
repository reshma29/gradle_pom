package week3.WedHW;

import org.openqa.selenium.chrome.ChromeDriver;

public class Checkbox {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver d=new ChromeDriver();
		d.manage().window().maximize();
		d.get("http://leafground.com/pages/checkbox.html");
		boolean b = d.findElementByXPath("//div[@class='example']/following::label[3]").isSelected();//.isEnabled();
		System.out.println(b);

	}

}
