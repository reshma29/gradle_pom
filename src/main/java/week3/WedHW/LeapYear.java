package week3.WedHW;

import java.util.Scanner;

public class LeapYear {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner S=new Scanner(System.in);		
		System.out.println("Enter a Year");
		int Year=S.nextInt();
		if((Year%4)==0)
		{
			if((Year%100 !=0)) {
				//Days=29;
				System.out.println(Year+" is a Leap Year");
				
			}else if((Year%100 ==0) && (Year%400 ==0)) {
				System.out.println(Year+" is a Leap Year");
			}else {
				System.out.println(Year+" is NOT a Leap Year");
			}
		}else {
			System.out.println(Year+" is NOT a Leap Year");
		}
	}

}
