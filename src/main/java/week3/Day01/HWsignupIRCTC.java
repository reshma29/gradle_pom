package week3.Day01;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HWsignupIRCTC {

	public static void main(String[] args) throws InterruptedException {
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver d=new ChromeDriver();
		d.manage().window().maximize();
		d.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
		d.findElementById("userRegistrationForm:userName").sendKeys("Practice_Sel");
		d.findElementById("userRegistrationForm:password").sendKeys("Test@987");
		d.findElementById("userRegistrationForm:confpasword").sendKeys("Test@987");
		
		WebElement securityQuestion=d.findElementById("userRegistrationForm:securityQ");
		Select dropDown = new Select(securityQuestion);
		dropDown.selectByVisibleText("What is your pet name?");
		d.findElementById("userRegistrationForm:securityAnswer").sendKeys("TestLeaf");
		
		WebElement preferredLanguage=d.findElementById("userRegistrationForm:prelan");
		Select dropDown2 = new Select(preferredLanguage);
		//dropDown2.selectByVisibleText("English");
		dropDown2.selectByValue("en");
		
		
		d.findElementById("userRegistrationForm:firstName").sendKeys("Selenium");
		d.findElementById("userRegistrationForm:middleName").sendKeys("August");
		d.findElementById("userRegistrationForm:lastName").sendKeys("Batch");
		d.findElementById("userRegistrationForm:gender:1").click();
		d.findElementById("userRegistrationForm:maritalStatus:1").click();
		
		WebElement dobDay =d.findElementById("userRegistrationForm:dobDay");
		Select dropDownDay = new Select(dobDay);
		dropDownDay.selectByVisibleText("18");
		
		WebElement dobMonth =d.findElementById("userRegistrationForm:dobMonth");
		Select dropDownMonth = new Select(dobMonth);
		dropDownMonth.selectByVisibleText("AUG");
		
		WebElement dobYear =d.findElementById("userRegistrationForm:dateOfBirth");
		Select dropDownYear = new Select(dobYear);
		dropDownYear.selectByVisibleText("2000");
		
		WebElement occupation =d.findElementById("userRegistrationForm:occupation");
		Select dropDownOcc = new Select(occupation);
		dropDownOcc.selectByVisibleText("Private");
		
		d.findElementById("userRegistrationForm:uidno").sendKeys("987654321123");
		d.findElementById("userRegistrationForm:idno").sendKeys("ASDFG1234G");
		
		WebElement country =d.findElementById("userRegistrationForm:countries");
		Select dropDownC = new Select(country);
		dropDownC.selectByVisibleText("India");
		
		d.findElementById("userRegistrationForm:email").sendKeys("testleaf.augsel@gmail.com");
		//d.findElementById("userRegistrationForm:isdCode").sendKeys("91"); //As it is disabled in UI, InvalidElementStateException occurred
		d.findElementById("userRegistrationForm:mobile").sendKeys("9876543210");
		
		WebElement nationality =d.findElementById("userRegistrationForm:nationalityId");
		Select dropDownN = new Select(nationality);
		//dropDownN.selectByVisibleText("India");
		dropDownN.selectByValue("94");
		
		d.findElementById("userRegistrationForm:address").sendKeys("4th Avenue");
		d.findElementById("userRegistrationForm:street").sendKeys("Gandhi Street");
		d.findElementById("userRegistrationForm:area").sendKeys("Chennai");
		d.findElementById("userRegistrationForm:pincode").sendKeys("603002",Keys.TAB);
		//d.findElementById("userRegistrationForm:statesName").sendKeys("603002",Keys.TAB);
		Thread.sleep(5000);
		WebElement city =d.findElementById("userRegistrationForm:cityName");
		Select dropDownCity = new Select(city);
		/*WebDriverWait wait = new WebDriverWait(d,60);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("userRegistrationForm:cityName")));
				//visibilityOf(city));		
				//elementToBeClickable(By.id("userRegistrationForm:cityName")));
		//List<WebElement> cityList = dropDownCity.getOptions();
		//int len = cityList.size();
*/		dropDownCity.selectByIndex(1); //StaleElementReferenceException Occured
				
		WebElement postOfc =d.findElementById("userRegistrationForm:postofficeName");
		Select dropDownPO = new Select(postOfc);
		List<WebElement> allOptions = dropDownPO.getAllSelectedOptions();
		//getFirstSelectedOption();
		//System.out.println(dropDownPO.getFirstSelectedOption());
		dropDownPO.selectByIndex(allOptions.size()-1);
		
		d.findElementById("userRegistrationForm:landline").sendKeys("603002",Keys.TAB);
		
		//d.close();
	}

}
