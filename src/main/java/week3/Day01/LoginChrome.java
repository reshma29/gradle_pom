package week3.Day01;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class LoginChrome {
	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");		
		ChromeDriver d= new ChromeDriver();
		try {			
			d.manage().window().maximize();
			d.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			
			
			d.get("http://leaftaps.com/opentaps/");
			
			d.findElementById("username").sendKeys("DemoSalesManager");
			try {
				d.findElementById("password").sendKeys("crmsfa");
			} catch (NoSuchElementException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			Thread.sleep(5000);
			d.findElementByClassName("decorativeSubmit").click();
			d.findElementByLinkText("CRM/SFA").click(); //should give value
			d.findElementByLinkText("Create Lead").click();
			d.findElementById("createLeadForm_companyName").sendKeys("Capgemini");
			d.findElementById("createLeadForm_firstName").sendKeys("Tapan");
			d.findElementById("createLeadForm_lastName").sendKeys("Reshma");
			
			WebElement element = d.findElementById("createLeadForm_dataSourceId");
			
			Select dropDown = new Select(element);
			dropDown.selectByVisibleText("Direct Mail");
			
			WebElement element2 = d.findElementById("createLeadForm_marketingCampaignId");
			Select dropDown2 = new Select(element2);
			List<WebElement> option =dropDown2.getOptions();
			
			int len = option.size();
			dropDown2.selectByIndex(len-2);
			
			WebElement element3 = d.findElementById("createLeadForm_currencyUomId");
			Select dropDown3 = new Select(element3);
			dropDown3.selectByValue("INR");
			d.findElementByName("submitButton").click();
		} catch (NoSuchElementException e) {			
			//e.printStackTrace();
			System.out.println("NoSuchElementException Thrown");
			//throw new RuntimeException();
			
		}
		finally{
			//d.close(); //closes only current tab, which was opened by Selenium
			//d.quit(); //closes whole browser
		}
		
		
	}

}
