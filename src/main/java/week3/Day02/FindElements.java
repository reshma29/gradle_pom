package week3.Day02;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class FindElements {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");		
		ChromeDriver d= new ChromeDriver();					
			d.manage().window().maximize();
			//d.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);			
			d.get("http://leafground.com/pages/table.html");
			/*List<WebElement> elements = d.findElements(By.xpath("//input[@type='checkbox']"));
			System.out.println(elements.size());
			int len = elements.size();			
			WebElement cb = elements.get(len-1);
			cb.click();*/
			
			WebElement elements2 = d.findElementByXPath("//section[@class='innerblock']//table");
			List<WebElement> tableRow = elements2.findElements(By.tagName("tr"));
			int rowSize = tableRow.size();
			System.out.println(rowSize);
			WebElement r = tableRow.get(rowSize-1);
			
			List<WebElement> tableCol = r.findElements(By.tagName("td"));
			int colSize = tableCol.size();
			System.out.println(colSize);
			WebElement c = tableCol.get(colSize-2);
			c.click();
			
			
			

	}

}
