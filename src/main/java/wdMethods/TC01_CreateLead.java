package wdMethods;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.pagefactory.internal.LocatingElementListHandler;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class TC01_CreateLead extends ProjectMethods{
	@BeforeTest
	public void setData() {
		testCaseName = "CreateLead";
		testCaseDesc="Creating a Lead";
		category="Smoke";
		author="Reshma";
	}
	@Test(invocationCount=2)
	public void CreateLead() {
		WebElement ClickCreateLead = locateElement("linkText", "Create Lead");
		click(ClickCreateLead);
		WebElement EnterCompanyName = locateElement("createLeadForm_companyName");
		type(EnterCompanyName, "Capgemini");
		WebElement EnterFirstName = locateElement("createLeadForm_firstName");
		type(EnterFirstName, "Reshma");
		WebElement EnterLastName = locateElement("createLeadForm_lastName");
		type(EnterLastName, "K");
		WebElement EnterFirstNameL = locateElement("createLeadForm_firstNameLocal");
		type(EnterFirstNameL, "Reshh");
		WebElement EnterLastNameL = locateElement("createLeadForm_lastNameLocal");
		type(EnterLastNameL, "K");
		WebElement EnterSalutation = locateElement("createLeadForm_personalTitle");
		type(EnterSalutation, "Miss");
		
		WebElement Sourcedd = locateElement("createLeadForm_dataSourceId");
		selectDropDownUsingIndex(Sourcedd, 2);
		
		WebElement EnterTitle = locateElement("createLeadForm_generalProfTitle");
		type(EnterTitle, "Student");
		WebElement EnterRevenue = locateElement("createLeadForm_annualRevenue");
		type(EnterRevenue, "17000");
		
		WebElement Industrydd = locateElement("createLeadForm_industryEnumId");
		selectDropDownUsingText(Industrydd, "Media");
		
		WebElement Email = locateElement("createLeadForm_primaryEmail");
		type(Email, "test@gmail.com");
		WebElement PhoneNum = locateElement("createLeadForm_primaryPhoneNumber");
		type(PhoneNum, "9597171727");
		
		WebElement CreateLeadButton = locateElement("name", "submitButton");
		click(CreateLeadButton);
		
		//throw new RuntimeException();
	}

}
