package wdMethods;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import freemarker.template.utility.CaptureOutput;
import wdMethods.ProjectMethods;

public class TC03_DeleteLead extends ProjectMethods{
	@BeforeTest
	public void setData() {
		testCaseName = "DeleteLead";
		testCaseDesc="Deleting an existing Lead";
		category="Smoke";
		author="Reshma";
	}
	@Test(dependsOnMethods={"wdMethods.TC01_CreateLead.CreateLead"})
	public void DeleteLead() throws InterruptedException  {
		WebElement ClickLead = locateElement("linkText", "Leads");
		click(ClickLead);
		WebElement ClickFindLeads = locateElement("linkText", "Find Leads");
		click(ClickFindLeads);
		WebElement ClickPhone = locateElement("xpath", "(//span[@class='x-tab-strip-text '])[2]");
		click(ClickPhone);
		WebElement EnterPhoneNum = locateElement("xpath", "//input[@name='phoneNumber']");
		type(EnterPhoneNum, "9597171727");
		WebElement FindLeadsButton = locateElement("xpath", "//button[text()='Find Leads']");
		click(FindLeadsButton);
		Thread.sleep(5000);
		takeSnap();
		WebElement FirstResultinglead = locateElement("xpath", "(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]/a");
		click(FirstResultinglead);
		
		WebElement DeleteButton = locateElement("xpath", "//a[@class='subMenuButtonDangerous']");
		click(DeleteButton);
		//Needs to be added from 14th row
		
	}
	
}
