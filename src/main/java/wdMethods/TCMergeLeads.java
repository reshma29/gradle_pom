package wdMethods;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class TCMergeLeads extends ProjectMethods{

	@BeforeTest
	public void setData() {
		testCaseName = "Merge";
		testCaseDesc="Merging 2 Leads";
		category="Smoke";
		author="Reshma";
	}
	@Test
	public void MergeLead() throws InterruptedException {		
		WebElement ClickLead = locateElement("linkText", "Leads");
		click(ClickLead);
		WebElement ClickMergeLead = locateElement("linkText", "Merge Leads");
		click(ClickMergeLead);
		WebElement FromLeadIcon = locateElement("xpath", "//input[@id='partyIdFrom']/following::img");
		click(FromLeadIcon);
		
		switchToWindow(1);
		WebElement FromLeadID = locateElement("name", "id");
		type(FromLeadID, "10");
		WebElement FromIDFindLead = locateElement("xpath", "//button[text()='Find Leads']");
		click(FromIDFindLead);
		Thread.sleep(5000);
		WebElement FirstResultingFromlead = locateElement("xpath", "(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]/a");
		String FromLeadText = getText(FirstResultingFromlead);
		click(FirstResultingFromlead);
		
		switchToWindow(0);
		WebElement ToLeadIcon = locateElement("xpath", "//input[@id='partyIdTo']/following::img");
		click(ToLeadIcon);
		
		switchToWindow(1);
		WebElement ToLeadID = locateElement("name", "id");
		type(ToLeadID, "101");
		WebElement ToIDFindLead = locateElement("xpath", "//button[text()='Find Leads']");
		click(ToIDFindLead);
		Thread.sleep(5000);
		WebElement FirstResultingTolead = locateElement("xpath", "(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]/a");
		click(FirstResultingTolead);
		
		switchToWindow(0);
		WebElement MergeButton = locateElement("linkText", "Merge");
		click(MergeButton);
		
		acceptAlert();
		WebElement ClickFindLeads = locateElement("linkText", "Find Leads");
		click(ClickFindLeads);		
		WebElement EnterFromLeadID = locateElement("name", "id");
		type(EnterFromLeadID, FromLeadText);
		WebElement CheckFrmIDFindLead = locateElement("xpath", "//button[text()='Find Leads']");
		click(CheckFrmIDFindLead);
		Thread.sleep(5000);
		WebElement msgText = locateElement("xpath", "//div[@class='x-paging-info']");
		//msgText.getText();
		System.out.println("*****"+msgText.getText()+"*****");
	}

}
