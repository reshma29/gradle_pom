package wdMethods;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class TC02_EditLead extends ProjectMethods{
	@BeforeTest
	public void setData() {
		testCaseName = "EditLead";
		testCaseDesc="Editing an existing Lead";
		category="Smoke";
		author="Reshma";
	}
	@Test(dependsOnMethods={"wdMethods.TC01_CreateLead.CreateLead"},enabled=false)
	public void EditLead() throws InterruptedException {
		WebElement ClickLead = locateElement("linkText", "Leads");
		click(ClickLead);
		WebElement ClickFindLeads = locateElement("linkText", "Find Leads");
		click(ClickFindLeads);
		WebElement EnterFirstName = locateElement("xpath","(//input[@name='firstName'])[3]");
		type(EnterFirstName, "Reshma");
		
		WebElement FindLeadsButton = locateElement("xpath", "//button[text()='Find Leads']");
		click(FindLeadsButton);
		Thread.sleep(5000);
		WebElement FirstResultinglead = locateElement("xpath", "(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]/a");
		click(FirstResultinglead);
		
		String title = getTitle();
		boolean verifyTitle =verifyTitle(title);
		//After this, throws WebDriverExceptionstale element reference: element is not attached to the page document
		
		WebElement EditButton = locateElement("linkText", "Edit");
		click(EditButton);
		
		WebElement EnterCompanyName = locateElement("updateLeadForm_companyName");
		type(EnterCompanyName, "Capggg");		
		
		WebElement UpdateButton = locateElement("xpath", "//input[@value='Update']");
		click(UpdateButton);
		
		String CheckCompanyName = getText(EnterCompanyName);
		WebElement VerifyUpadation = locateElement("viewLead_companyName_sp");
		verifyPartialText(VerifyUpadation, CheckCompanyName);
		
	}
}
