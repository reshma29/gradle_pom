package wdMethods;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class TC04_DuplicateLead extends ProjectMethods{
	@BeforeTest
	public void setData() {
		testCaseName = "DuplicateLead";
		testCaseDesc="Duplicating an existing Lead";
		category="Smoke";
		author="Reshma";
	}
	@Test
	public void DuplicateLead() throws InterruptedException {
		WebElement ClickLead = locateElement("linkText", "Leads");
		click(ClickLead);
		WebElement ClickFindLeads = locateElement("linkText", "Find Leads");
		click(ClickFindLeads);
		/*WebElement ClickEmail = locateElement("xpath", "(//span[@class='x-tab-strip-text '])[3]");
		click(ClickEmail);
		WebElement EnterEmail = locateElement("xpath", "//input[@name='emailAddress']");
		type(EnterEmail, "test@gmail.com");*/
		WebElement ClickPhone = locateElement("xpath", "(//span[@class='x-tab-strip-text '])[2]");
		click(ClickPhone);
		WebElement EnterPhoneNum = locateElement("xpath", "//input[@name='phoneNumber']");
		type(EnterPhoneNum, "9597171727");
		WebElement FindLeadsButton = locateElement("xpath", "//button[text()='Find Leads']");
		click(FindLeadsButton);
		Thread.sleep(5000);
		takeSnap();
		WebElement FirstResultinglead = locateElement("xpath", "(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]/a");
		click(FirstResultinglead);
		WebElement DuplicateButton = locateElement("linkText", "Duplicate Lead");
		click(DuplicateButton);
		
		boolean verifyTitle = verifyTitle("Duplicate Lead");
		
		WebElement CreateLLeadButton = locateElement("xpath", "//input[@value='Create Lead']");
		click(CreateLLeadButton);
		//Needs to be added 16th row 
		
	}
}
