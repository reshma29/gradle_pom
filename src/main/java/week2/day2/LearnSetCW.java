package week2.day2;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public class LearnSetCW {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Set<String> phoneSet = new LinkedHashSet<String>();
		phoneSet.add("Xperia");
		phoneSet.add("Samsung");		
		phoneSet.add("Asus");
		phoneSet.add("Samsung");
		phoneSet.add("Nokia");
				
		int size = phoneSet.size();
		System.out.println(size);
		for (String eachItem : phoneSet) {
			System.out.println("Printing individual Item in the set : "+eachItem);
		}
		
		//Using LIST as we don't have get in SET
		List<String> lst=new ArrayList<String>();
		lst.addAll(phoneSet);
		String firstItem = lst.get(0);
		System.out.println("Printing 1st Item in the set = "+firstItem);	
		
	}

}
