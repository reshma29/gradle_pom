package week2.day2;

import java.util.HashMap;
import java.util.Map;

public class LearnMap {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Map<Character, Integer> nameMap=new HashMap<Character, Integer>();
		//nameMap.PUT
		String S="Reshma";
		char allChar[]=S.toCharArray();
		for (char c : allChar) {
			if(nameMap.containsKey(c)){
				Integer val=nameMap.get(c)+1;
				nameMap.put(c, val);
				
				//System.out.println(c);
			}else {
				nameMap.put(c, 1);				
			}				
		}
		System.out.println("No duplicates"+nameMap);
		//if value !=1 print char

	}

}
