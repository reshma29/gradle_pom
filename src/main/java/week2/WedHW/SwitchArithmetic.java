package week2.WedHW;

import java.util.Scanner;

public class SwitchArithmetic {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Enter 1st input");
		Scanner S=new Scanner(System.in);
		int ip1=S.nextInt();
		System.out.println("Enter 2nd input");
		//Scanner S=new Scanner(System.in);
		int ip2=S.nextInt();
		System.out.println("Enter any Operation(Add,Subtract,Multiply,Divide) to perform ");
		//String ip3=S.nextLine(); //Advances this scanner past the current line and returns the input that was skipped.
		String ip3=S.next();  //Finds and returns the next complete token from this scanner.
		switch (ip3)
		{
			case "Add":
				System.out.println("Add: "+(ip1+ip2));	
				break;
			case "Subtract":
				System.out.println("Subtract: "+(ip1-ip2));	
				break;
			case "Multiply":
				System.out.println("Multiply: "+(ip1*ip2));	
				break;
			case "Divide":
				if ((ip1>ip2) && (ip2 !=0))
				{
					System.out.println("Division: "+(ip1/ip2));					
				}else {
					System.out.println("2nd input should be LESS than 1st input and not be ZERO");
				}				
				break;
			default: 
				System.out.println("Inavalid Operation");
		}
		
	}

}
