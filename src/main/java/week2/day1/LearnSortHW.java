package week2.day1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class LearnSortHW {

	public static void main(String[] args) {		
			
		List<Character> sortChar=new ArrayList<Character>();
		String S="Amazon India";
		char allChar[]=S.toCharArray();	//Convert the given String into character Array as we are using arraylist
				
		for (char c : allChar) {
			sortChar.add(c);		//To add each character to arraylist	
		}
		Collections.sort(sortChar); //To sort
		System.out.println("sorted characters = "+sortChar); 		
		
		//sortChar.equalsIgnoreCase(sortChar);
		sortChar.clear();
		String CaseIgnore=S.toLowerCase();
		char finalChar[]=CaseIgnore.toCharArray();
		
		for (char charIgnore : finalChar) {
			sortChar.add(charIgnore);
		}
		System.out.println("lower characters = "+sortChar);
		Collections.sort(sortChar);
		System.out.println("lower sorted characters = "+sortChar);
		Collections.reverse(sortChar);
		System.out.println("reverse sorted character ="+sortChar);
		//To remove duplicates
		int Dup=finalChar.length;
		//Dup=removeDuplicates(finalChar,Dup); //should create 'removeDuplicates' method to call
		
		for (int i=0; i<Dup; i++)
	           System.out.print(finalChar[i]+" ");
		
		
		
		
	}

}
