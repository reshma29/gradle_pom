package week2.day1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class LearnArray {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		List<String> allPhones=new ArrayList<String>();
		allPhones.add("Asus");
		allPhones.add("OnePlus");
		allPhones.add("Moto");
		allPhones.add("LG");
		allPhones.add("honor");
		allPhones.add("Mi");
		//To display all Mobile Names
		for (String eachPhone : allPhones) {
			System.out.println(eachPhone);
		} 
		System.out.println("************");	 
		//To display count of all Mobile Phones
		int size = allPhones.size();
		System.out.println("count= "+ size);
		
		System.out.println("************");	 
		//To print last but one Mobile Name
		//AllPhones.get(size-2);
		System.out.println("last"+(allPhones.get(size-2)));
		
		System.out.println("************");	 		
		//Print in ASCII order
		Collections.sort(allPhones);
		System.out.println(allPhones);
						
	}

}
