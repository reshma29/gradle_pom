package week4.day1;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.poi.util.SystemOutLogger;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.chrome.ChromeDriver;

public class HWMergeLead {

	public static void main(String[] args) throws InterruptedException, IOException {
		// TODO Auto-generated method stub
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");		
		ChromeDriver d= new ChromeDriver();

		d.manage().window().maximize();
		d.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		d.get("http://leaftaps.com/opentaps/");

		d.findElementById("username").sendKeys("DemoSalesManager");
		d.findElementById("password").sendKeys("crmsfa");
		Thread.sleep(5000);
		d.findElementByClassName("decorativeSubmit").click();
		d.findElementByLinkText("CRM/SFA").click(); //should give value
		d.findElementByLinkText("Leads").click();
		d.findElementByLinkText("Merge Leads").click();
		d.findElementByXPath("//input[@id='partyIdFrom']/following::img").click();
		
		Set<String> windowHandles = d.getWindowHandles();
		List<String> windowsList=new ArrayList <>();
		windowsList.addAll(windowHandles);
		
		d.switchTo().window(windowsList.get(1));
		System.out.println(d.getTitle());		
		d.findElementByName("id").sendKeys("10");
		d.findElementByXPath("//button[text()='Find Leads']").click();
		//d.findElementByClassName("linktext").click();//got WebDriverException
		/*String text = d.findElementByXPath("//div[@class='x-grid3-cell-inner x-grid3-col-partyId']").getText(); 
		System.out.println(text); -- output is 10091, which is not fom the filtered records */
		//d.navigate().refresh();
		Thread.sleep(5000);
		d.findElementByXPath("(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]/a").click();		
		//System.out.println(d.getTitle()); //NoSuchWindowException: no such window: window was already closed				
		
		d.switchTo().window(windowsList.get(0));
		System.out.println(d.getTitle());	
		d.findElementByXPath("//input[@id='partyIdTo']/following::img").click();
		
		windowHandles = d.getWindowHandles();
		windowsList=new ArrayList <>();
		windowsList.addAll(windowHandles);
		
		d.switchTo().window(windowsList.get(1));
		System.out.println(d.getTitle());
		d.findElementByName("id").sendKeys("101");
		d.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(5000);
		d.findElementByXPath("(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]/a").click();
		
		d.switchTo().window(windowsList.get(0));
		System.out.println(d.getTitle());
		d.findElementByLinkText("Merge").click();
		
		d.switchTo().alert().accept();
		d.findElementByLinkText("Find Leads").click();
		d.findElementByName("id").sendKeys("10236");
		d.findElementByXPath("//button[text()='Find Leads']").click();
		//new window, instead of error Msg. 
		File snap = d.getScreenshotAs(OutputType.FILE);
		File Path=new File("./SNAPS/MergeTC25.png");
		FileUtils.copyFile(snap, Path);
	}

}
