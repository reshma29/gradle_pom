package week4.day1;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class HWTestCases {

	public static void main(String[] args) throws InterruptedException {
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver d= new ChromeDriver();		
		d.manage().window().maximize();
		d.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		d.get("http://leaftaps.com/opentaps/");

		d.findElementById("username").sendKeys("DemoSalesManager");
		d.findElementById("password").sendKeys("crmsfa");
		Thread.sleep(3000);
		d.findElementByClassName("decorativeSubmit").click();
		d.findElementByLinkText("CRM/SFA").click(); //should give value
		
		d.findElementByLinkText("Create Lead").click();
		d.findElementById("createLeadForm_companyName").sendKeys("Selenium");
		WebElement FirstName= d.findElementById("createLeadForm_firstName");
		FirstName.sendKeys("Test");
		d.findElementById("createLeadForm_lastName").sendKeys("Leaf");
		d.findElementById("createLeadForm_firstNameLocal").sendKeys("August");
		d.findElementById("createLeadForm_lastNameLocal").sendKeys("Batch");
		d.findElementById("createLeadForm_personalTitle").sendKeys("Miss");
		
		WebElement dd = d.findElementById("createLeadForm_dataSourceId");
		Select Sourcedd=new Select(dd);
		Sourcedd.selectByVisibleText("Conference");
		
		d.findElementById("createLeadForm_generalProfTitle").sendKeys("Student");
		d.findElementById("createLeadForm_annualRevenue").sendKeys("17000");
		
		WebElement dd1 = d.findElementById("createLeadForm_industryEnumId");
		Select Industrydd=new Select(dd1);
		Industrydd.selectByVisibleText("Computer Software");
		
		WebElement dd2 = d.findElementById("createLeadForm_ownershipEnumId");
		Select Ownershipdd=new Select(dd2);
		Ownershipdd.selectByVisibleText("Sole Proprietorship");
		
		d.findElementById("createLeadForm_sicCode").sendKeys("9211");
		d.findElementById("createLeadForm_description").sendKeys("Private Industry");
		d.findElementById("createLeadForm_importantNote").sendKeys("Important");
		d.findElementById("createLeadForm_primaryPhoneCountryCode").sendKeys("91");
		d.findElementById("createLeadForm_primaryPhoneAreaCode").sendKeys("08778");
		d.findElementById("createLeadForm_primaryPhoneExtension").sendKeys("234567");
		d.findElementById("createLeadForm_departmentName").sendKeys("Dept");
		
		WebElement dd3 = d.findElementById("createLeadForm_currencyUomId");
		Select Currencydd=new Select(dd3);
		Currencydd.selectByVisibleText("INR - Indian Rupee");
		
		d.findElementById("createLeadForm_numberEmployees").sendKeys("3000");
		d.findElementById("createLeadForm_tickerSymbol").sendKeys("$@$");
		d.findElementById("createLeadForm_primaryPhoneAskForName").sendKeys("Pavithra");
		d.findElementById("createLeadForm_primaryWebUrl").sendKeys("https://www.google.com/");
		
		d.findElementById("createLeadForm_generalAddress1").sendKeys("OMR Road");
		d.findElementById("createLeadForm_generalAddress2").sendKeys("Ste 4");
		d.findElementById("createLeadForm_generalCity").sendKeys("Chennai");
		
		WebElement dd4 = d.findElementById("createLeadForm_generalCountryGeoId");
		Select Countrydd=new Select(dd4);
		Countrydd.selectByVisibleText("India");		
		WebElement dd5 = d.findElementById("createLeadForm_generalStateProvinceGeoId");
		Select Statedd=new Select(dd5);
		Statedd.selectByVisibleText("TAMILNADU");		
		
		d.findElementById("createLeadForm_generalPostalCode").sendKeys("603002");
		d.findElementById("createLeadForm_generalPostalCodeExt").sendKeys("123");
		
		WebElement dd6 = d.findElementById("createLeadForm_marketingCampaignId");
		Select MarkCampdd=new Select(dd6);
		MarkCampdd.selectByVisibleText("Automobile");
		
		d.findElementById("createLeadForm_primaryPhoneNumber").sendKeys("9876541231");
		d.findElementById("createLeadForm_primaryEmail").sendKeys("test@gmail.com");
		d.findElementByName("submitButton").click();
		
		WebElement verifyName = d.findElementById("viewLead_firstName_sp");
		String name = verifyName.getText();
		System.out.println(name);
		if (FirstName.getText().equals(name)) {
			System.out.println("Matched");
		}else {
			System.out.println("Not Matched");
		}
		
		d.close();
	}
}
