package practice;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadExcel {
	public static Object [][]  exceldata(String excelFileName) throws IOException {
		XSSFWorkbook wbook=new XSSFWorkbook("./Excel_data/"+excelFileName+".xlsx");
		XSSFSheet sheet=wbook.getSheetAt(0);
		//XSSFSheet sheet=wbook.getSheet("CreateLead");		
		int rowCount = sheet.getLastRowNum();
		System.out.println("No. of Rows: "+rowCount);
		int colCount = sheet.getRow(0).getLastCellNum();
		System.out.println("No. of Columns: "+colCount);
		Object [][] data=new Object [rowCount][colCount];
		for (int i = 1; i <= rowCount; i++) {
			XSSFRow rowNum = sheet.getRow(i);
			for (int j = 0; j < colCount; j++) {
				XSSFCell cellNum = rowNum.getCell(j);
				/*String cellValue = cellNum.getStringCellValue();
				System.out.println("cell value of "+i+j +": "+ cellValue);*/
				data [i-1][j] = cellNum.getStringCellValue();
			}
		}
		return data;
		
	}

}
