package practice;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class TC02_EditLead extends ProjectMethods_DataPro{
	@BeforeTest(groups= {"smoke","sanity","regression"})
	public void setData() {
		testCaseName = "EditLead";
		testCaseDesc="Editing an existing Lead";
		category="Smoke";
		author="Reshma";
		excelFileName="EditLead";
	}
	//@Test(groups= {"sanity"},dependsOnGroups= {"smoke"})
	@Test(dataProvider="fetchExcelData")
	public void EditLead(String fName,String companyName) throws InterruptedException {
		WebElement ClickLead = locateElement("linkText", "Leads");
		click(ClickLead);
		WebElement ClickFindLeads = locateElement("linkText", "Find Leads");
		click(ClickFindLeads);
		WebElement EnterFirstName = locateElement("xpath","(//input[@name='firstName'])[3]");
		type(EnterFirstName, fName);
		
		WebElement FindLeadsButton = locateElement("xpath", "//button[text()='Find Leads']");
		click(FindLeadsButton);
		Thread.sleep(5000);
		WebElement FirstResultinglead = locateElement("xpath", "(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]/a");
		click(FirstResultinglead);
		
		String title = getTitle();
		boolean verifyTitle=verifyTitle(title);
		System.out.println("Edit Title - "+title);
		System.out.println(verifyTitle);
		//After this, throws WebDriverExceptionstale element reference: element is not attached to the page document
		
		WebElement EditButton = locateElement("linkText", "Edit");
		click(EditButton);
		
		WebElement EnterCompanyName = locateElement("updateLeadForm_companyName");
		type(EnterCompanyName, companyName);		
		
		WebElement UpdateButton = locateElement("xpath", "//input[@value='Update']");
		click(UpdateButton);
		
		String CheckCompanyName = getText(EnterCompanyName);
		WebElement VerifyUpadation = locateElement("viewLead_companyName_sp");
		verifyPartialText(VerifyUpadation, CheckCompanyName);
		//System.out.println(VerifyUpadation);		
	}
	/*@DataProvider(name="positive")
	public Object [][] fetchData() throws IOException {
		Object [][] data=new Object [2][2] ;
		data[0][0]="Reshma";
		data[0][1]="Igate";
		data[1][0]="Nadhiya";
		data[1][1]="CapGem";
		return data;
		return ReadExcel.exceldata("EditLead");
	}*/

}
