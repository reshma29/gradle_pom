package practice;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.pagefactory.internal.LocatingElementListHandler;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class TC01_CreateLead extends ProjectMethods_DataPro{
	@BeforeTest(groups= {"common"})
	public void setData() {
		testCaseName = "CreateLead";
		testCaseDesc="Creating a Lead";
		category="Smoke";
		author="Reshma";
		excelFileName="CreateLead";
	}
	//@Test//(groups= {"smoke"})
	@Test(dataProvider="fetchExcelData")
	public void CreateLead(String cName,String fName,String lName,String email, String pNum ) {
		WebElement ClickCreateLead = locateElement("linkText", "Create Lead");
		click(ClickCreateLead);
		WebElement EnterCompanyName = locateElement("createLeadForm_companyName");
		type(EnterCompanyName, cName);
		WebElement EnterFirstName = locateElement("createLeadForm_firstName");
		type(EnterFirstName, fName);
		WebElement EnterLastName = locateElement("createLeadForm_lastName");
		type(EnterLastName, lName);
		WebElement EnterFirstNameL = locateElement("createLeadForm_firstNameLocal");
		type(EnterFirstNameL, "Reshh");
		WebElement EnterLastNameL = locateElement("createLeadForm_lastNameLocal");
		type(EnterLastNameL, "K");
		WebElement EnterSalutation = locateElement("createLeadForm_personalTitle");
		type(EnterSalutation, "Miss");
		
		WebElement Sourcedd = locateElement("createLeadForm_dataSourceId");
		selectDropDownUsingIndex(Sourcedd, 2);
		
		WebElement EnterTitle = locateElement("createLeadForm_generalProfTitle");
		type(EnterTitle, "Student");
		WebElement EnterRevenue = locateElement("createLeadForm_annualRevenue");
		type(EnterRevenue, "17000");
		
		WebElement Industrydd = locateElement("createLeadForm_industryEnumId");
		selectDropDownUsingText(Industrydd, "Media");
		
		WebElement Email = locateElement("createLeadForm_primaryEmail");
		type(Email, email);
		WebElement PhoneNum = locateElement("createLeadForm_primaryPhoneNumber");
		type(PhoneNum, pNum);
		
		WebElement CreateLeadButton = locateElement("name", "submitButton");
		click(CreateLeadButton);
		
		//throw new RuntimeException();
	}
	/*@DataProvider(name="fetchExcelData")
	public Object [][] fetchData() throws IOException {
		Object [][] data=new Object [2][2] ;
		data[0][0]="Reshma";
		data[0][1]="8866641277";
		data[1][0]="ReshmaTest";
		data[1][1]="9876543210";
		return data;
		
		return ReadExcel.exceldata("CreateLead");
	}*/

}
