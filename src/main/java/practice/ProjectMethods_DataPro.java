package practice;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;

import wdMethods.SeMethods;

public class ProjectMethods_DataPro extends SeMethods{
	@BeforeSuite(groups= {"common"})
	public void beforeSuite() {
		beginResult();
	}
	@BeforeClass(groups= {"common"})
	public void beforeClass() {
		startTestCase();
	}
	
	@BeforeMethod(groups= {"common"})
	@Parameters({"url","username","password"})
	public void login(String url, String username, String password) throws InterruptedException {
		startApp("chrome", url);
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName,username );
		WebElement elePassword = locateElement("id","password");
		type(elePassword, password);
		Thread.sleep(3000);
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin);
		WebElement eleCRM = locateElement("linkText","CRM/SFA");
		click(eleCRM);
	}
	@AfterMethod(groups= {"common"})
	public void closeApp() {
		closeBrowser();
	}
	@AfterSuite(groups= {"common"})
	public void afterSuite() {
		endResult();
	}
	
	@DataProvider(name="fetchExcelData")
	public Object [][] fetchData() throws IOException {
				
		return ReadExcel.exceldata(excelFileName);
	}
	
	
	
}
