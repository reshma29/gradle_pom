package week1.DailyHWTue;

import java.util.Scanner;

public class Factorial {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.out.println("Enter a number to know its factorial");
		Scanner R=new Scanner(System.in);
		int n=R.nextInt();
		int fact=1;
		while(n>0)
		{
			fact=fact*n;
			n--;				
		}
		System.out.println(fact);
	}

}
