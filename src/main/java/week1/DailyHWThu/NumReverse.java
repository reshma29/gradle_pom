package week1.DailyHWThu;

import java.util.Scanner;

public class NumReverse {

	public static void main(String[] args) {
		
		Scanner S=new Scanner(System.in);
		System.out.println("Enter a Number");  
		int Num=S.nextInt();
		int ReversedNum=0;
		int R;
		
		while(Num>0)
		{
			R=Num%10;
			ReversedNum=(ReversedNum*10)+R;
			Num=Num/10;			
		}
		System.out.println("The Reversed Number is "+ReversedNum);

	}

}
