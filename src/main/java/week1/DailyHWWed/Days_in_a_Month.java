package week1.DailyHWWed;

import java.util.Scanner;

public class Days_in_a_Month {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner S=new Scanner(System.in);
		System.out.println("Enter a Month number");
		int Monthnum = S.nextInt();
		String MonthName="";
		int Days=0;
		System.out.println("Enter a Year");
		int Year=S.nextInt();
		switch(Monthnum)
		{
		case 1:
			MonthName = "January";
			Days  = 31;
			System.out.println(Monthnum+"st month of the Year is "+MonthName);
			System.out.println("Number of Days in the month of "+MonthName+" is "+Days);
			break;
		case 2:
			MonthName = "February";
			if((Year%4)==0)
			{
				if((Year%100 !=0)) {
					Days=29;
				}else if((Year%100 ==0) && (Year%400 ==0)) {
					Days  = 29;
				}else {
					Days=28;
				}
			}else {
				Days=28;
			}
			System.out.println(Monthnum+"nd month of the Year is "+MonthName);
			System.out.println("Number of Days in the month of "+MonthName+" is "+Days);
			break;
		case 3:
			MonthName = "March";
			Days  = 31;
			System.out.println(Monthnum+"rd month of the Year is "+MonthName);
			System.out.println("Number of Days in the month of "+MonthName+" is "+Days);
			break;
		case 4:
			MonthName = "April";
			Days  = 30;
			System.out.println(Monthnum+"th month of the Year is "+MonthName);
			System.out.println("Number of Days in the month of "+MonthName+" is "+Days);
			break;
		case 5:
			MonthName = "May";
			Days  = 31;
			System.out.println(Monthnum+"th month of the Year is "+MonthName);
			System.out.println("Number of Days in the month of "+MonthName+" is "+Days);
			break;
		case 6:
			MonthName = "June";
			Days  = 30;
			System.out.println(Monthnum+"th month of the Year is "+MonthName);
			System.out.println("Number of Days in the month of "+MonthName+" is "+Days);
			break;
		case 7:
			MonthName = "July";
			Days  = 31;
			System.out.println(Monthnum+"th month of the Year is "+MonthName);
			System.out.println("Number of Days in the month of "+MonthName+" is "+Days);
			break;
		case 8:
			MonthName = "August";
			Days  = 31;
			System.out.println(Monthnum+"th month of the Year is "+MonthName);
			System.out.println("Number of Days in the month of "+MonthName+" is "+Days);
			break;
		case 9:
			MonthName = "September";
			Days  = 30;
			System.out.println(Monthnum+"th month of the Year is "+MonthName);
			System.out.println("Number of Days in the month of "+MonthName+" is "+Days);
			break;
		case 10:
			MonthName = "October";
			Days  = 31;
			System.out.println(Monthnum+"th month of the Year is "+MonthName);
			System.out.println("Number of Days in the month of "+MonthName+" is "+Days);
			break;
		case 11:
			MonthName = "November";
			Days  = 30;
			System.out.println(Monthnum+"th month of the Year is "+MonthName);
			System.out.println("Number of Days in the month of "+MonthName+" is "+Days);
			break;
		case 12:
			MonthName = "December";
			Days  = 31;
			System.out.println(Monthnum+"th month of the Year is "+MonthName);
			System.out.println("Number of Days in the month of "+MonthName+" is "+Days);
			break;	
		default:
			System.out.println("Invalid Month Number");
			break;
		}
		
	}

}
