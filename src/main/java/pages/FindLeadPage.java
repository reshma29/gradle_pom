package pages;

import org.openqa.selenium.WebElement;

public class FindLeadPage extends ProjectMethods_POM{
	
	public FindLeadPage () {
		
	}
	
	public FindLeadPage typeLeadID(String data) {
		WebElement eleLeadID = locateElement("xpath", "(//input[@name='id'])");
		type(eleLeadID, data);
		return this;
	}
	public  FindLeadPage typeLeadFName(String data) {
		WebElement eleLeadFName = locateElement("xpath","(//input[@name='firstName'])[3]");
		type(eleLeadFName, data);
		return this;
	}
	public FindLeadPage typeLeadLName(String data) {
		WebElement eleLeadLName = locateElement("xpath", "(//input[@name='lastName'])[3]");
		type(eleLeadLName, data);
		return this;
	}
	public FindLeadPage typeLeadCompName(String data) {
		WebElement eleLeadCompName = locateElement("xpath", "(//input[@name='companyName'])[2]");
		type(eleLeadCompName, data);
		return this;
	}
	public FindLeadPage clickFindLeadsButton() throws InterruptedException {
		WebElement eleFindLeadtButton = locateElement("xpath", "//button[text()='Find Leads']");
		click(eleFindLeadtButton);
		Thread.sleep(3000);
		return this;		
	}
	
	public ViewLeadPage clickResultantFindLead () {
		WebElement eleFirstResultingFlead = locateElement("xpath", "(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]/a");
		click(eleFirstResultingFlead);
		return new ViewLeadPage();
	}

}
