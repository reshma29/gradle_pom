package pages;

import org.openqa.selenium.WebElement;

import cucumber.api.java.en.Then;

public class ViewLeadPage extends ProjectMethods_POM{
	
	public ViewLeadPage() {
		
	}
	@Then("verify Lead Creation is success as (.*)")
	public ViewLeadPage verifyFirstName(String expectedText) {
		WebElement vFirstName = locateElement("xpath", "//span[@id='viewLead_firstName_sp']");
		verifyPartialText(vFirstName, expectedText);
		System.out.println("Successfully created a Lead");
		return this;
	}
	public DuplicateLeadPage clickDuplicateLead() {
		WebElement eleDupButton = locateElement("linkText", "Duplicate Lead");
		click(eleDupButton);
		return new DuplicateLeadPage();
	}
	public EditLeadPage clickEdit() {
		WebElement eleEditButton = locateElement("linkText", "Edit");
		click(eleEditButton);
		return new EditLeadPage();
	}
	public MyLeadsPage clickDelete() {
		WebElement eleDelButton = locateElement("xpath", "//a[@class='subMenuButtonDangerous']");
		click(eleDelButton);
		return new MyLeadsPage();
	}
	
	public LoginPage clickLogOut() {
		WebElement eleLogOut = locateElement("linkText","Logout");
		click(eleLogOut);
		return new LoginPage();
	}
	

}
