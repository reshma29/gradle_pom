package pages;

import org.openqa.selenium.WebElement;

import cucumber.api.java.en.And;

public class MyLeadsPage extends ProjectMethods_POM{
	
	public MyLeadsPage() {
		
	}
	@And("click CreateLead link")
	public CreateLeadPage clickCreateLeads() {
		WebElement eleCreateLead = locateElement("linkText","Create Lead");
		click(eleCreateLead);
		return new CreateLeadPage();
	}
	
	public FindLeadPage clickFindLeads() {
		WebElement eleFindLeads = locateElement("linkText", "Find Leads");
		click(eleFindLeads);
		return new FindLeadPage();
	}

}
