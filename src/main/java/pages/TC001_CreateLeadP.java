package pages;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class TC001_CreateLeadP extends ProjectMethods_POM{
	
	@BeforeTest(groups= {"common"})
	public void setData() {
		testCaseName = "TC001_CreateLeadP";
		testCaseDesc="Creating a Lead through POM";
		category="Smoke";
		author="Reshma";
		excelFileName="CreateLeadPage";
	}

	@Test(dataProvider="fetchExcelData")
	public void CreateLead(String uName,String Pwd,String cName,String fName,String lName,String vFName) throws InterruptedException {
		new LoginPage()
		.typeUserName(uName)
		.typePassword(Pwd)
		.clickLogin()
		.clickCRMSFA()
		.clickLeads()
		.clickCreateLeads()
		.typeCompanyName(cName)
		.typeFirstName(fName)
		.typeLastName(lName)
		.clickCreateLead()
		.verifyFirstName(vFName)
		.clickLogOut();		
	}
}
