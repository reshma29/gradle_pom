package pages;

import org.openqa.selenium.WebElement;

import cucumber.api.java.en.And;

public class MyHomePage extends ProjectMethods_POM{
	
	public MyHomePage() {
		
	}
	@And("click Leads link")
	public MyLeadsPage clickLeads() {
		WebElement eleLeads = locateElement("linkText","Leads");
		click(eleLeads);
		return new MyLeadsPage();
	}

}
