package pages;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class TC002_EditLeadP extends ProjectMethods_POM{
	
	@BeforeTest(groups= {"common"})
	public void setData() {
		testCaseName = "TC002_EditLeadP";
		testCaseDesc="Editing a Lead through POM";
		category="Smoke";
		author="Reshma";
		excelFileName="EditLeadPage";
	}

	@Test(dataProvider="fetchExcelData")
	public void CreateLead(String uName,String Pwd,String leadID,String cNameEdit) throws InterruptedException {
		new LoginPage()
		.typeUserName(uName)
		.typePassword(Pwd)
		.clickLogin()
		.clickCRMSFA()
		.clickLeads()
		.clickFindLeads()
		.typeLeadID(leadID)
		.clickFindLeadsButton()
		.clickResultantFindLead()
		.clickEdit()
		.typeEditCName(cNameEdit)
		.clickUpdate()
		.clickLogOut();		
	}
}

