package pages;

import org.openqa.selenium.WebElement;

import cucumber.api.java.en.And;
import cucumber.api.java.en.When;

public class CreateLeadPage extends ProjectMethods_POM{
	
	public CreateLeadPage() {
	
	}
	@And("Enter Company Name as (.*)")
	public CreateLeadPage typeCompanyName(String data) {
		WebElement eleCompName = locateElement("id", "createLeadForm_companyName");
		type(eleCompName, data);
		return this;
	}
	@And("Enter First Name as (.*)")
	public CreateLeadPage typeFirstName(String data) {
		WebElement eleFName = locateElement("id", "createLeadForm_firstName");
		type(eleFName, data);
		return this;
	}
	@And("Enter Last Name as (.*)")
	public CreateLeadPage typeLastName(String data) {
		WebElement eleLName = locateElement("id", "createLeadForm_lastName");
		type(eleLName, data);
		return this;
	}
	@When("click Create Lead Button")
	public ViewLeadPage clickCreateLead() {
		WebElement eleCreateLead = locateElement("name", "submitButton");
		click(eleCreateLead);
		return new ViewLeadPage();
	}
	
	
	

}
