package pages;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class TC004_DuplicateLeadP extends ProjectMethods_POM{
	
	@BeforeTest(groups= {"common"})
	public void setData() {
		testCaseName = "TC002_DuplicateLeadP";
		testCaseDesc="Duplicate a Lead through POM";
		category="Smoke";
		author="Reshma";
		excelFileName="DuplicateLeadPage";
	}

	@Test(dataProvider="fetchExcelData")
	public void CreateLead(String uName,String Pwd,String leadCompName) throws InterruptedException {
		new LoginPage()
		.typeUserName(uName)
		.typePassword(Pwd)
		.clickLogin()
		.clickCRMSFA()
		.clickLeads()
		.clickFindLeads()
		.typeLeadCompName(leadCompName)
		.clickFindLeadsButton()
		.clickResultantFindLead()
		.clickDuplicateLead()
		.clickDupCreateLead()
		.clickLogOut();		
	}

}
