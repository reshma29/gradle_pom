package pages;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class TC003_DeleteLeadP extends ProjectMethods_POM{
	
	@BeforeTest(groups= {"common"})
	public void setData() {
		testCaseName = "TC002_DeleteLeadP";
		testCaseDesc="Deleting a Lead through POM";
		category="Smoke";
		author="Reshma";
		excelFileName="DeleteLeadPage";
	}

	@Test(dataProvider="fetchExcelData")
	public void CreateLead(String uName,String Pwd,String leadCompName) throws InterruptedException {
		new LoginPage()
		.typeUserName(uName)
		.typePassword(Pwd)
		.clickLogin()
		.clickCRMSFA()
		.clickLeads()
		.clickFindLeads()
		.typeLeadCompName(leadCompName)
		.clickFindLeadsButton()
		.clickResultantFindLead()
		.clickDelete();		
		//.clickLogOut();		
	}

}
