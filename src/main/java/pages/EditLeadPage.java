package pages;

import org.openqa.selenium.WebElement;

public class EditLeadPage extends ProjectMethods_POM{
	public EditLeadPage() {
		
	}
	
	public EditLeadPage typeEditCName(String data) {
		WebElement eleEditCompName = locateElement("updateLeadForm_companyName");
		type(eleEditCompName, data);
		return this;
	}
	public ViewLeadPage clickUpdate() {
		WebElement eleUpdateButton = locateElement("xpath", "//input[@value='Update']");
		click(eleUpdateButton);
		return new ViewLeadPage();
	}

}
