package pages;

import org.openqa.selenium.WebElement;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;

public class HomePage extends ProjectMethods_POM{
	
	public HomePage() {
		
	}
	@Then("verify Login is Success as (.*)")
	public HomePage verifyUserName(String expectedText) {
		WebElement vUserName = locateElement("xpath", "//h2[text()[contains(.,'Demo')]]");
		verifyPartialText(vUserName, expectedText);
		System.out.println("Login Success");
		return this;
	}
	
	public HomePage clickHomeLogOut() {
		WebElement eleHLogOut = locateElement("class", "decorativeSubmit");
		click(eleHLogOut);
		return this;
	}
	@And("click CRMSFA link")
	public MyHomePage clickCRMSFA() {
		WebElement eleCRMSFA = locateElement("linkText", "CRM/SFA");
		click(eleCRMSFA);
		return new MyHomePage();
	}
		
}
