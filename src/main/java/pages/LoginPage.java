package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import cucumber.api.java.en.When;
import practice.ProjectMethods_DataPro;

public class LoginPage extends ProjectMethods_DataPro{
	
	public LoginPage() {
		PageFactory.initElements(driver, this);
	}
	@FindBy(how=How.ID,using="username")
	WebElement eleUserName;
	@And("Enter User Name as (.*)")
	public LoginPage typeUserName(String data) {
		//WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, data);
		return this;
	}
	@FindBy(id="password")
	WebElement elePassword;
	@And("Enter Password as (.*)")
	public LoginPage typePassword(String data) throws InterruptedException {
		WebElement elePassword = locateElement("id", "password");
		type(elePassword, data);
		Thread.sleep(3000);
		return this;
	}
	@FindBy(className="decorativeSubmit")
	WebElement eleLogin;
	@When("click Login button")
	public HomePage clickLogin() {
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin);
		return new HomePage();
	}
	/*@FindBy(className="decorativeSubmit")
	WebElement eleFLogin;
	@When("click Login button")
	public HomePage clickFailedLogin() {
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin);
		return new HomePage();
	}*/
	
}
	
	
	