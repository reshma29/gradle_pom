package pages;

import org.openqa.selenium.WebElement;

public class DuplicateLeadPage extends ProjectMethods_POM{
	
	public DuplicateLeadPage() {
		
	}
	
	public ViewLeadPage clickDupCreateLead() {
		WebElement DupCreateLeadButton = locateElement("linkText", "Create Lead");
		click(DupCreateLeadButton);
		return new ViewLeadPage();
	}

}
