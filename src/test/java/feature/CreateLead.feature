Feature: Creating a Lead in LeafTaps

#Background:
#    	Given Open Chrome Browser 
#		And maximize the Browser
#		And Set Timeouts
#		And Hit URL

@Positive @Negative
Scenario Outline: Positive CreateLead Flow
And Enter User Name as <userName>
And Enter Password as <password>
When click Login button
Then verify Login is Success as <vUserName>
And click CRMSFA link 
And click Leads link
And click CreateLead link  
And Enter Company Name as <companyName>
And Enter First Name as <firstName>
And Enter Last Name as <LastName>
When click Create Lead Button 
Then verify Lead Creation is success as <vLeadName> 

Examples:
|userName|password|vUserName|companyName|firstName|LastName|vLeadName|
|DemosalesManager|crmsfa|Demo Sales Manager|TestLeaf|Reshh|K|Reshh|
|DemoCSR|crmsfa|Demo B2B CSR|Selenium|Reshma|Reshh|Reshma|

@Negative
Scenario: Negative Login Flow
And Enter User Name as DemosalesManager
And Enter Password as crmsfa
When click Login button 
But verify Login is Failed
