package steps;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import wdMethods.SeMethods;


public class Hooks extends SeMethods{
	@Before
	public void beforeScenario(Scenario scBefore) {		
		System.out.println(scBefore.getName());
		System.out.println(scBefore.getId());
		beginResult();
		testCaseName = scBefore.getName();
		testCaseDesc=scBefore.getId();
		category="Smoke";
		author="Reshma";
		//excelFileName="CreateLeadPage";
		startTestCase();
		startApp("chrome", "http://leaftaps.com/opentaps/");
	}
	@After
	public void AfterScenario(Scenario scAfter) {
		System.out.println(scAfter.getStatus());
		System.out.println(scAfter.isFailed());
		closeAllBrowsers();
		endResult();
	}
}
